/*
    biobambam2
    Copyright (C) 2009-2015 German Tischler
    Copyright (C) 2011-2015 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/fastx/FastAStreamSet.hpp>
#include <libmaus2/util/ArgInfo.hpp>

#include <biobambam2/BamBamConfig.hpp>
#include <biobambam2/Licensing.hpp>

int getDefaultVerbose()
{
	return 0;
}

int populaterefcache(libmaus2::util::ArgInfo const & arginfo)
{
	if ( getenv("REF_CACHE") == NULL )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] REF_CACHE env variable is unset" << std::endl;
		lme.finish();
		throw lme;
	}

	for ( uint64_t i = 0; i < arginfo.getNumRestArgs(); ++i )
	{
		std::string const fn = arginfo.getUnparsedRestArg(i);
		libmaus2::aio::InputStreamInstance ISI(fn);
		libmaus2::fastx::FastAStreamSet FASS(ISI);

		std::map<std::string,std::string> const M = FASS.computeMD5(true,true);

		for ( std::map<std::string,std::string>::const_iterator it = M.begin(); it != M.end(); ++it )
			std::cout << fn << "\t" << it->first << "\t" << it->second << std::endl;
	}

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgInfo const arginfo(argc,argv);

		for ( uint64_t i = 0; i < arginfo.restargs.size(); ++i )
			if (
				arginfo.restargs[i] == "-v"
				||
				arginfo.restargs[i] == "--version"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				return EXIT_SUCCESS;
			}
			else if (
				arginfo.restargs[i] == "-h"
				||
				arginfo.restargs[i] == "--help"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				std::cerr << std::endl;
				std::cerr << "Key=Value pairs:" << std::endl;
				std::cerr << std::endl;

				std::vector< std::pair<std::string,std::string> > V;

				V.push_back ( std::pair<std::string,std::string> ( "verbose=<["+::biobambam2::Licensing::formatNumber(getDefaultVerbose())+"]>", "print progress report" ) );

				::biobambam2::Licensing::printMap(std::cerr,V);

				std::cerr << std::endl;
				return EXIT_SUCCESS;
			}

		return populaterefcache(arginfo);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
