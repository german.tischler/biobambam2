/**
    biobambam2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <biobambam2/BamBamConfig.hpp>
#include <biobambam2/Licensing.hpp>

#include <libmaus2/bambam/BamCatHeader.hpp>
#include <libmaus2/bambam/BamBlockWriterBaseFactory.hpp>
#include <libmaus2/bambam/BamMultiAlignmentDecoderFactory.hpp>
#include <libmaus2/util/stringFunctions.hpp>

#include <iomanip>
#include <iostream>

#include <libmaus2/util/ArgParser.hpp>

#include <config.h>

int bamheadercat(libmaus2::util::ArgParser const & /* arg */)
{
	std::vector<std::string> Vfn;
	std::string line;
	while ( std::getline(std::cin,line) )
		if ( line.size() )
			Vfn.push_back(line);

	libmaus2::bambam::BamCatHeader BCH(Vfn);

	std::cout << BCH.bamheader->text;

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		std::vector<libmaus2::util::ArgParser::ArgumentDefinition> const Vformatin = libmaus2::bambam::BamAlignmentDecoderInfo::getArgumentDefinitions();
		std::vector<libmaus2::util::ArgParser::ArgumentDefinition> Vformat = Vformatin;

		::libmaus2::util::ArgParser arg(argc,argv,Vformat);

		int const r = bamheadercat(arg);

		return r;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
